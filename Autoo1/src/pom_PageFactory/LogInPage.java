package pom_PageFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.*;


public class LogInPage {
	WebDriver driver;
	
	@FindBy(xpath=".//*//div[@id=\"root\"]//input[@id = \"formHorizontalEmail\"]")
	public WebElement txtEmail;
	
	@FindBy(xpath=".//*//input[@id = \"formHorizontalPassword\"]")
	public WebElement txtPassword;
	
	@FindBy(xpath=".//*//div[@id=\"root\"]//div[@class=\"signin-container__form__login\"]//a[@class=\"col-login__btn\"]")
	public WebElement btnLogIn;
	
	

	public LogInPage(WebDriver driver){
		this.driver = driver;
	    //This initElements method will create all WebElements
	    PageFactory.initElements(driver, this);
	    }
	 
	private void inputText(WebElement obj, String txt) {
		obj.click();
		obj.sendKeys(txt);
		}
	
	public void loginAnAccount(String Email, String Password) {
		inputText(txtEmail, Email);
		inputText(txtPassword, Password);
		btnLogIn.click();
		}
}