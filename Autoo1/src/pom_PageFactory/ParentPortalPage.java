package pom_PageFactory;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.*;

public class ParentPortalPage {
WebDriver driver;
	
	@FindBy(xpath=".//*//div[@id=\"root\"]//button[@class=\"btn btn-filter module_grid__btn_filter btn btn-default\"]")
	public WebElement btnFilters;
	
	
	//Element on the Filters popup
	@FindBy(xpath=".//*//div[@role=\"dialog\"]//select[@id=\"formControlsSelect\"]")
	public WebElement drpRequestStatus;
	
	@FindBy(xpath=".//*//div[@role=\"dialog\"]//div[@class=\"modal-content\"]//button[@class=\"btn-filter btn btn-default\"]")
	public WebElement btnApplyFilters;
	
	@FindBy(xpath=".//*//div[@id=\"root\"]//table//th[@title=\"First Name\"]")
	public WebElement btnSortFirstName;
	
	@FindBy(xpath=".//*//div[@id=\"root\"]//div[@class=\"react-bs-container-body\"]//tbody")
	public WebElement tblData;
	
	@FindBy(xpath=".//*//div[@id=\"root\"]//div[@class=\"react-bs-container-body\"]//tbody//td[6]")
	public List<WebElement> fistNameList;
	
	public void verifySortFistName() {
		verifySort(fistNameList, "First Name");
	}
	
	private void verifySort(List<WebElement> objElements, String nameColumn) {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); 
		LinkedList<String> dataList = new LinkedList<String>();
        int size = objElements.size();

        for(WebElement temp:objElements)
        {
            String sTemp=temp.getText();
           // to remove extra spaces and to Lowercase Every product name**
            dataList.add(sTemp.toLowerCase().trim());           
        }
        
        //Check Sorted Or NOt?
        Boolean num = checkAscendingOrder(dataList);
        
        if(num == true)
        {

            System.out.println(nameColumn + " are Sorted Order");
        }
        else {
            System.out.println(nameColumn+ " are not Sorted Order");
            }
        
	}
	
	private static boolean checkAscendingOrder(LinkedList<String> Names){
	    String previous = "";

	    for (String current: Names) {
	        if (current.compareTo(previous) < 0){
	            return false;
	        }
	        previous = current;
	    }
	    return true;
	    }   

	
	private void selectRequestStatus(String status) {
		btnFilters.click();
		drpRequestStatus.click();
		Select drpReqStatus = new Select(drpRequestStatus);
		drpReqStatus.selectByVisibleText(status);
		}
	
	public void filterByRequestStatus(String status) {
		selectRequestStatus(status);
		btnApplyFilters.click();
		}
	
	public ParentPortalPage(WebDriver driver){
		this.driver = driver;
	    //This initElements method will create all WebElements
	    PageFactory.initElements(driver, this);
	    }
}

