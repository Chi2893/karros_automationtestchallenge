
import java.util.concurrent.TimeUnit;
import pom_PageFactory.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class Auto_main {

	public static void main(String[] args) throws Exception
	{
		LogInPage objLogInPage;
		ParentPortalPage objParentPortalPage;
		
		String url = "http://ktvn-test.s3-website.us-east-1.amazonaws.com/";		  		 
		System.setProperty("webdriver.gecko.driver","./geckodriver.exe");
		  
		WebDriver driver = new FirefoxDriver(); 
		//Puts an Implicit wait, Will wait for 5 seconds before throwing exception
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS); 
		  
		//Launch the test site 
		driver.navigate().to(url);
		objLogInPage = new LogInPage(driver);
		String Email = "admin@test.com";
		String Password = "test123";
		objLogInPage.loginAnAccount(Email, Password);
		  
		//Filter Student Access Request with INACTIVE
		objParentPortalPage = new ParentPortalPage(driver);
		String status = "Inactive";
		objParentPortalPage.filterByRequestStatus(status);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		Thread.sleep(5000);
		
		//Verify sorting of First Name column
		objParentPortalPage.verifySortFistName();
		
		//Close the Browser
		driver.close();	
	}

}
